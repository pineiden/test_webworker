import { TestWebWorkerPage } from './app.po';

describe('test-web-worker App', () => {
  let page: TestWebWorkerPage;

  beforeEach(() => {
    page = new TestWebWorkerPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
