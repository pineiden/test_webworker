import { Component } from '@angular/core';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent {
    title = 'app';
    circle_on = true
    circle2_on = true

    togleCircle() {
        console.log(this.circle_on)
        if (this.circle_on) {
            this.circle_on = false
        }
        else {
            this.circle_on = true
        }
        return this.circle_on
    }

    togleCircle2() {
        console.log(this.circle2_on)
        if (this.circle2_on) {
            this.circle2_on = false
        }
        else {
            this.circle2_on = true
        }
        return this.circle2_on
    }

}
