import { WorkerAppModule } from '@angular/platform-webworker';
//import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { APP_BASE_HREF, Location } from '@angular/common';
import { ColorPickerModule } from 'angular4-color-picker';

import { AppComponent } from './app.component';

@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        WorkerAppModule,
        HttpModule,
        ColorPickerModule

        //BrowserModule
    ],
    providers: [

    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
